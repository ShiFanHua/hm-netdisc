package cn.jasonone.hm.netdisc;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.TextNode;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.*;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipOutputStream;

@SpringBootTest
class HmNetdiscApplicationTests {
	
	public static void main(String[] args) throws IOException {
//		Document doc=Jsoup.parse(new URL("http://www.mca.gov.cn/article/sj/xzqh/2020/2020/202007170301.html"), 15000);
//		Elements trs = doc.select("tr[height=19]");
//		Map<String,String> citys = new HashMap<>();
//		String prefix="";
//		for (Element tr : trs) {
//			Elements select = tr.select(".xl704942");
//			if(select.isEmpty()){
//				select = tr.select(".xl714942");
//				Element id = select.get(0);
//				Element name = select.get(1);
//				List<TextNode> textNodes = name.textNodes();
//				citys.put(id.text(), prefix+" "+textNodes.get(0).text().trim());
//			}else{
//				prefix=select.get(1).textNodes().get(0).text().trim();
//			}
//		}
//
////		List<String> list = Files.readAllLines(Paths.get("E:\\2002\\level3\\hm-netdisc\\areaCode"));
//		StringBuffer sb=new StringBuffer();
//		for (Map.Entry<String,String> s : citys.entrySet()) {
//			sb.append("AREA_CODE_MAP.put(\"")
//					.append(s.getKey())
//					.append("\",\"")
//					.append(s.getValue())
//					.append("\");\n");
//		}
//		System.out.println(sb);
		File file=new File("files/60e44632364cdceb7c17d48f66d4139d");
		ZipOutputStream zop=new ZipOutputStream(new FileOutputStream(new File("files/a.zip")));
		ZipEntry zipEntry0=new ZipEntry("a.jpg");
		zop.putNextEntry(zipEntry0);
		Files.copy(file.toPath(), zop);
		zop.closeEntry();
		zop.flush();
//		zop.write();
		ZipEntry zipEntry1=new ZipEntry("一级目录/a.jpg");
		zop.putNextEntry(zipEntry1);
		Files.copy(file.toPath(), zop);
		zop.closeEntry();
		zop.flush();
		ZipEntry zipEntry2=new ZipEntry("一级目录/二级目录/a.jpg");
		zop.putNextEntry(zipEntry2);
		Files.copy(file.toPath(), zop);
		zop.closeEntry();
		zop.flush();
		zop.close();
	}
	
	
//	private void write(ZipOutputStream zipOutputStream,File file){
//		try(FileInputStream fis=new FileInputStream(file)){
//			byte[] buff=new byte[1024];
//
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		} catch (IOException e) {
//			e.printStackTrace();
//		}
//	}
	
}
