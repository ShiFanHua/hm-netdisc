/*
 Navicat Premium Data Transfer

 Source Server         : localhost_3306
 Source Server Type    : MySQL
 Source Server Version : 80012
 Source Host           : localhost:3306
 Source Schema         : 2002

 Target Server Type    : MySQL
 Target Server Version : 80012
 File Encoding         : 65001

 Date: 27/07/2020 12:04:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for data_dictionary
-- ----------------------------
DROP TABLE IF EXISTS `data_dictionary`;
CREATE TABLE `data_dictionary`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键 唯一标识',
  `code` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分组 数据分组',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '名称 数据名称',
  `value` varchar(3072) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '数据 数据的值',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '数据字典 数据字典表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of data_dictionary
-- ----------------------------
INSERT INTO `data_dictionary` VALUES (1, 'file_type', 'jpg', '4');
INSERT INTO `data_dictionary` VALUES (2, 'file_type', 'wmv', '21');
INSERT INTO `data_dictionary` VALUES (3, 'file_type', 'zip', '23');

-- ----------------------------
-- Table structure for disk_info
-- ----------------------------
DROP TABLE IF EXISTS `disk_info`;
CREATE TABLE `disk_info`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '标识符;文件ID',
  `user_id` int(11) NOT NULL COMMENT '用户ID;唯一标识',
  `pid` int(11) NOT NULL COMMENT '父文件;所属父目录',
  `name` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '文件名;文件名',
  `path` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文件的MD5签名',
  `type` int(11) NULL DEFAULT 0 COMMENT '文件类型;文件类型',
  `directory` bit(1) NOT NULL COMMENT '是否是目录;是否是目录',
  `size` bigint(20) NULL DEFAULT 0 COMMENT '文件大小;文件的大小',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 11 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '磁盘信息;用于存储用户的文件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of disk_info
-- ----------------------------
INSERT INTO `disk_info` VALUES (10, 2, 9, 'Burner-1080.jpg', '60e44632364cdceb7c17d48f66d4139d', 0, b'0', 3160918, 0, 0, '2020-07-24 16:11:36', '2020-07-24 16:11:36');
INSERT INTO `disk_info` VALUES (11, 2, -1, 'Burner-4k.jpg', '714ef7ada25faf9988d0b4e65ad42b07', 4, b'0', 5574531, 0, 0, '2020-07-27 10:09:09', '2020-07-27 10:09:09');
INSERT INTO `disk_info` VALUES (12, 2, -1, 'logo.zip', 'bf4111773443e7b61fc94572cfde7cea', 4, b'0', 4446, 0, 0, '2020-07-27 10:16:26', '2020-07-27 10:16:26');
INSERT INTO `disk_info` VALUES (13, 2, -1, 'Java', NULL, 0, b'1', 0, 0, 0, '2020-07-27 11:15:07', '2020-07-27 11:15:07');
INSERT INTO `disk_info` VALUES (14, 2, 13, 'IO', NULL, 0, b'1', 0, 0, 0, '2020-07-27 11:59:46', '2020-07-27 11:59:46');

-- ----------------------------
-- Table structure for disk_space
-- ----------------------------
DROP TABLE IF EXISTS `disk_space`;
CREATE TABLE `disk_space`  (
  `id` int(11) NOT NULL COMMENT '主键;唯一标识',
  `user_id` int(11) NOT NULL COMMENT '用户ID;空间所属用户',
  `size` bigint(20) NOT NULL COMMENT '空间大小;空间大小',
  `type` int(11) NOT NULL COMMENT '空间类型;空间类型',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `Expire_date` datetime(0) NOT NULL COMMENT '到期时间;空间到期时间',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '磁盘空间信息;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for file_type
-- ----------------------------
DROP TABLE IF EXISTS `file_type`;
CREATE TABLE `file_type`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键;标识号',
  `magic_number` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文件魔数;文件魔数',
  `name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '类型名称;文件类型名称',
  `icon` int(11) NOT NULL COMMENT '图标;文件类型图标',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  `UPDATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '文件类型表;' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of file_type
-- ----------------------------
INSERT INTO `file_type` VALUES (1, '25504446', 'Adobe Illustrator', 1, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (2, '424D', 'Bitmap graphic', 2, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (3, 'CAFEBABE', 'Class File', 3, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (4, 'FFD8', 'JPEG graphic file', 4, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (5, '0000000C6A5020200D0A', 'JPEG 2000 graphic file', 5, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (6, '47494638', 'GIF graphic file', 6, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (7, '4949', 'TIF graphic file', 7, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (8, '89504E47', 'PNG graphic file', 8, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (9, '52494646', 'WAV audio file', 9, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (10, '7F454C46', 'ELF Linux EXE', 10, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (11, '38425053', 'Photoshop Graphics', 11, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (12, 'D7CDC69A', 'Windows Meta File', 12, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (13, '4D546864', 'MIDI file', 13, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (14, '00000100', 'Icon file', 14, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (15, '494433', 'MP3 file with ID3 identity tag', 15, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (16, '52494646', 'AVI video file', 16, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (17, '465753', 'Flash Shockwave', 17, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (18, '464C56', 'Flash Video', 18, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (19, '00000018667479706D703432', 'Mpeg 4 video file', 19, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (20, '6D6F6F76', 'MOV video file', 20, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (21, '3026B2758E66CF', 'Windows Video file', 21, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (22, '3026B2758E66CF', 'Windows Audio file', 22, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (23, '504B0304', 'PKZip', 23, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (24, '1F8B08', 'GZip', 24, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (25, '7573746172', 'Tar file', 25, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (26, 'D0CF11E0A1B11AE1', 'Microsoft Installer', 26, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (27, '4C01', 'Object Code File', 27, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (28, '4D5A', 'Dynamic Library', 28, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (29, '4D534346', 'CAB Installer file', 29, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (30, '4D5A', 'Executable file', 30, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (31, '526172211A0700', 'RAR file', 31, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (32, '4D5A', 'SYS file', 32, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (33, '3F5F0300', 'Help file', 33, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (34, '4B444D56', 'VMWare Disk file', 34, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (35, '2142444E42', 'Outlook Post Office file', 35, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (36, '25504446', 'PDF Document', 36, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (37, 'D0CF11E0A1B11AE1', 'Word Document', 37, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (38, '7B5C72746631', 'RTF Document', 38, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (39, 'D0CF11E0A1B11AE1', 'Excel Document', 39, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (40, 'D0CF11E0A1B11AE1', 'PowerPoint Document', 40, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (41, 'D0CF11E0A1B11AE1', 'Visio Document', 41, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (42, '504B0304', 'DOCX (Office 2010)', 42, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (43, '504B0304', 'XLSX (Office 2010)', 43, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (44, '504B0304', 'PPTX (Office 2010)', 44, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (45, '5374616E64617264204A6574', 'Microsoft Database', 45, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (46, '2521', 'Postcript File', 46, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (47, 'D0CF11E0A1B11AE1', 'Outlook Message File', 47, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (48, '252150532D41646F62652D332E3020455053462D332030', 'EPS File', 48, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (49, '504B0304140008000800', 'Jar File', 49, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (50, '4D6963726F736F66742056697375616C2053747564696F20536F6C7574696F6E2046696C65', 'SLN File', 50, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (51, '789C', 'Zlib File', 51, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');
INSERT INTO `file_type` VALUES (52, '789C', 'SDF File', 52, 0, 0, '2020-07-27 09:48:41', '2020-07-27 09:48:41');

-- ----------------------------
-- Table structure for login_record
-- ----------------------------
DROP TABLE IF EXISTS `login_record`;
CREATE TABLE `login_record`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键 唯一标识',
  `user_id` int(11) NOT NULL COMMENT '用户ID 登录的用户',
  `ip` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录IP 当前登录的IP信息',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录地点 当前登录的地点,通过IP进行获取',
  `state` int(1) NOT NULL DEFAULT 0 COMMENT '状态 状态',
  `logically_delete` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 逻辑删除字段,1为删除',
  `LOGIN_TIME` datetime(0) NOT NULL COMMENT '登录时间 当前登录的时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '登录记录 用户登录记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_information
-- ----------------------------
DROP TABLE IF EXISTS `member_information`;
CREATE TABLE `member_information`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `member_information_type` int(11) NOT NULL COMMENT '会员类型;会员类型',
  `experience` int(11) NOT NULL COMMENT '经验;会员当前产生的经验总额',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `FIRST_TIME` datetime(0) NOT NULL COMMENT '首次开通时间;首次开通会员的时间',
  `LAST_TIME` datetime(0) NOT NULL COMMENT '最后一次开通时间;最近一次开通会员的时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '会员信息;存储用户的会员信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for member_information_record
-- ----------------------------
DROP TABLE IF EXISTS `member_information_record`;
CREATE TABLE `member_information_record`  (
  `id` int(11) NOT NULL COMMENT '主键;唯一标识',
  `user_id` int(11) NOT NULL COMMENT '用户ID;用户ID',
  `member_infomation_type` int(11) NOT NULL COMMENT '会员类型;会员类型',
  `start_time` datetime(0) NOT NULL COMMENT '会员开始时间;会员新的开始时间',
  `deadline` datetime(0) NOT NULL COMMENT '会员到期时间;会员新的截止时间',
  `IP` char(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录IP;开通时登录的IP地址',
  `address` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '登录地点;开通时登录的地点',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '会员记录;开通会员的记录表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for pdman_db_version
-- ----------------------------
DROP TABLE IF EXISTS `pdman_db_version`;
CREATE TABLE `pdman_db_version`  (
  `DB_VERSION` varchar(256) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `VERSION_DESC` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL,
  `CREATED_TIME` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pdman_db_version
-- ----------------------------
INSERT INTO `pdman_db_version` VALUES ('v0.0.0', '默认版本，新增的版本不能低于此版本', '2020-07-20 11:06:41');
INSERT INTO `pdman_db_version` VALUES ('v1.0.1', '初始化', '2020-07-20 11:06:46');
INSERT INTO `pdman_db_version` VALUES ('v1.0.2', '将所有的创建时间的默认值更改为当前时间', '2020-07-20 12:19:37');

-- ----------------------------
-- Table structure for user_info
-- ----------------------------
DROP TABLE IF EXISTS `user_info`;
CREATE TABLE `user_info`  (
  `ID` int(11) NOT NULL AUTO_INCREMENT COMMENT '主键',
  `nick_name` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称;用户的昵称',
  `account` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '账号;登录使用的账号',
  `password` char(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码;登录使用的密码,存储密文',
  `email` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '邮箱;用户邮箱',
  `Avatar` int(11) NOT NULL COMMENT '头像;头像ID',
  `Identity` varchar(18) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '身份证;用户的身份证信息',
  `sex` int(1) NOT NULL COMMENT '性别;用户性别',
  `native_place` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '籍贯;用户的籍贯',
  `date_birth` date NOT NULL COMMENT '出生日期;用户的出生日期',
  `state` int(1) NOT NULL COMMENT '状态;用户的状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '注册时间',
  `UPDATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) ON UPDATE CURRENT_TIMESTAMP(0) COMMENT '更新时间;用户的最后修改时间',
  PRIMARY KEY (`ID`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '用户信息表;存储用户的个人信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_info
-- ----------------------------
INSERT INTO `user_info` VALUES (2, '管理员', 'admin', '202cb962ac59075b964b07152d234b70', '784442670@qq.com', 1, '110101199003070214', 1, '湖南', '2020-07-22', 0, 0, '2020-07-22 15:48:12', '2020-07-22 16:39:12');
INSERT INTO `user_info` VALUES (4, '1', '1', 'c4ca4238a0b923820dcc509a6f75849b', '1@qq.com', 1, '110101199003075891', 1, '北京市 东城区', '1990-03-07', 0, 0, '2020-07-23 16:09:47', '2020-07-23 16:09:47');

-- ----------------------------
-- Table structure for wallet
-- ----------------------------
DROP TABLE IF EXISTS `wallet`;
CREATE TABLE `wallet`  (
  `user_id` int(11) NOT NULL COMMENT '用户ID',
  `balance` decimal(32, 8) NOT NULL DEFAULT 0.00000000 COMMENT '余额 钱包余额',
  `state` int(1) NOT NULL DEFAULT 0 COMMENT '状态 状态',
  `logically_delete` int(1) NOT NULL DEFAULT 0 COMMENT '逻辑删除 逻辑删除字段,1为删除',
  `UPDATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '更新时间',
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '钱包 钱包信息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Table structure for wallet_record
-- ----------------------------
DROP TABLE IF EXISTS `wallet_record`;
CREATE TABLE `wallet_record`  (
  `id` int(11) NOT NULL COMMENT '主键;唯一标识',
  `user_id` int(11) NOT NULL COMMENT '用户ID;所属用户的ID',
  `old_balance` decimal(32, 8) NOT NULL COMMENT '变更前的余额;变更前的金额',
  `new_balance` decimal(32, 8) NOT NULL COMMENT '变更后的余额;变更后的金额',
  `change_amount` decimal(32, 8) NOT NULL COMMENT '改变金额;改变的金额',
  `consumption_type` int(11) NOT NULL COMMENT '消费类型;消费类型ID',
  `state` int(1) NOT NULL COMMENT '状态;状态',
  `logically_delete` int(1) NOT NULL COMMENT '逻辑删除;逻辑删除字段,1为删除',
  `CREATED_TIME` datetime(0) NOT NULL DEFAULT CURRENT_TIMESTAMP(0) COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '消费记录;消费记录' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
