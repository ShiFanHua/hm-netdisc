class DisplayElement {
    _display;

    constructor(elem, visable) {
        if (elem instanceof HTMLElement) {
            this.elem = elem;
        } else if (typeof (elem) === 'string') {
            this.elem = document.querySelector(elem);
        } else {
            throw "非法的元素对象";
        }

        if (visable != undefined) {
            this.visable = visable;
        }

    }

    addEventListener(eventName, listener) {
        this.elem.addEventListener(eventName, e => {
            if (this.visable) {
                listener.call(this.elem, e);
            }
        });
    }

    set visable(visable) {
        if (!visable) {
            if(this.elem.style.display!='none'){
                this._display = this.elem.style.display;
                this.elem.style.display = 'none';
            }
        } else {
            this.elem.style.display = this._display;
        }
        this.elem.hidden = !visable;

    }

    get visable() {
        return !this.elem.hidden;
    }
}

// <div class="layui-btn-container" style="padding-left: 10px;padding-top: 10px;">
//     <span class="layui-breadcrumb preDir" lay-separator="|">
//         <a pid="-1">返回上一级&emsp;|&emsp;</a>
//         <a ></a>
//     </span>
//     <span class="layui-breadcrumb dirList" lay-separator=">">
//         <a pid="-1"><cite>全部文件</cite></a>
//     </span>
// </div>
class Breadcrumb {
    pathList = {
        elem: document.createElement("span"),
        length: 0,
        listeners: [],
        addClickListener(listener) {
            this.listeners.push(listener);
        },
        push(pid, a) {
            let lastPath = this.elem.lastChild;
            if (lastPath) {
                lastPath.innerHTML = lastPath.innerText;
            }
            a.innerHTML = `<cite>${a.innerHTML}</cite>`;
            a.addEventListener("click", e => {
                for (let listener of this.listeners) {
                    listener.call(a, e, pid);
                }
            })
            this.elem.append(a);
            this.length++;
            layui.use("element", () => {
                this.elem.querySelectorAll("span").forEach(s => s.remove());
                layui.element.init();
            })
        },
        pop(pid) {
            let list = [];
            this.elem.querySelectorAll("a").forEach(n => list.push(n));
            if (list.length == 1) {
                return;
            }
            if (isNaN(pid)) {
                list.pop().remove();
                this.length--;
            } else {
                while (list.length > 1) {
                    let a = list.pop();
                    if (a.getAttribute("pid") == pid) {
                        list.push(a);
                        break;
                    } else {
                        a.remove();
                        this.length--;
                    }
                }
            }
            let lastPath = list[list.length - 1];
            lastPath.innerHTML = `<cite>${lastPath.innerHTML}</cite>`;
            layui.use("element", () => {
                this.elem.querySelectorAll("span").forEach(s => s.remove());
                layui.element.init();
            })

            return lastPath.getAttribute("pid");
        }
    };
    _pid;

    constructor(element) {
        this.elem = element;
        this._initBack();
        this._initPathList();
        this.addPathClickListener((e,pid)=>{
            this.pop(pid);
        });
    }

    _initPathList() {
        this.pathList.elem.classList.add("layui-breadcrumb");
        this.pathList.elem.setAttribute("lay-separator", ">");
        this.elem.append(this.pathList.elem);
    }

    _initBack() {
        let span = document.createElement("span");
        span.classList.add("layui-breadcrumb");
        span.setAttribute("lay-separator", "|");

        let a = document.createElement("a");
        a.innerHTML = "返回上一级";
        a.addEventListener("click", e => {
            let pid = this.pop();
            for (let backListener of this.pathList.listeners) {
                backListener.call(a, e, pid);
            }
        })
        span.append(a);
        span.append(document.createElement("a"));
        this.elem.append(span);
        this._back = new DisplayElement(span);
    }

    addPathClickListener(listener) {
        this.pathList.addClickListener(listener);
    }

    addPath(pid, name) {
        let a = document.createElement("a");
        a.setAttribute("pid", pid);
        a.innerHTML = name;
        this.pathList.push(pid, a);
        if (this.pathList.length == 1) {
            this._back.visable = false;
        } else {
            this._back.visable = true;
        }
    }

    pop(pid) {
        pid = this.pathList.pop(pid);
        if (this.pathList.length == 1) {
            this._back.visable = false;
        } else {
            this._back.visable = true;
        }
        return pid;
    }
}
Notification.requestPermission(function(state){
    Notification.permission=state;
})
function downFile(src) {
    let a = document.createElement("a");
    a.href = src;
    a.click();
    if(Notification.permission=='granted'){
        new Notification("温馨提示",{body:"即将开始下载文件",tag:'download'});
    }
}