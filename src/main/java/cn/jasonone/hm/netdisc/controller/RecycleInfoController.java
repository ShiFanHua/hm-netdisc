package cn.jasonone.hm.netdisc.controller;

import cn.jasonone.hm.netdisc.config.HmConfig;
import cn.jasonone.hm.netdisc.consts.SystemConst;
import cn.jasonone.hm.netdisc.model.dto.HttpResult;
import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;
import cn.jasonone.hm.netdisc.service.RecycleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @author Jason
 * @date 2020-07-23 16:20:07
 */
@Controller
@RequestMapping("/recycleInfo")
@Slf4j
public class RecycleInfoController {
    @Resource
    private RecycleService recycleService;

    @GetMapping(produces = MediaType.TEXT_HTML_VALUE)
    public String home() {
        return "recycleInfo/index";
    }

    @GetMapping
    @ResponseBody
    public HttpResult findAll(DiskInfo diskInfo, @SessionAttribute(SystemConst.SESSION_LOGIN_STATUS) UserInfo userInfo) {
        if (diskInfo == null || diskInfo.getPid() == null) {
            if (diskInfo == null) {
                diskInfo = new DiskInfo();
            }
            diskInfo.setPid(-1);
        }
        if (diskInfo.getUserId() == null) {
            diskInfo.setUserId(userInfo.getId());
        }
        diskInfo.setLogicallyDelete(true);
        List<DiskInfo> list = recycleService.findAll(diskInfo);
        log.debug(String.valueOf(list));
        return HttpResult.success("查询成功").setData(list);
    }

    @PostMapping("/recover")
    @ResponseBody
    public HttpResult Recover(@RequestParam("ids[]") Integer[] id) {

        if (id == null || id.length == 0) {
            log.error("恢复失败", id);
            return HttpResult.create(500, "参数传入失败");
        }
        recycleService.recover(id);
        return HttpResult.success("恢复成功");
    }

    @PostMapping("/delete")
    @ResponseBody
    public HttpResult delete(@RequestParam("ids[]") Integer[] id) {
//		recycleService.deleteByIds(true,id);
        if (id == null || id.length == 0) {
            log.error("删除失败", id);
            return HttpResult.create(500, "参数传入失败");
        }
        this.recycleService.deleteByIds(true, id);
        return HttpResult.success("删除成功");
    }
}
