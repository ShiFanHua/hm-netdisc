package cn.jasonone.hm.netdisc.controller;

import cn.jasonone.hm.netdisc.model.dto.UserInfoDTO;
import cn.jasonone.hm.netdisc.model.dto.UserInfoITO;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;
import cn.jasonone.hm.netdisc.model.vo.UserInfoVO;
import cn.jasonone.hm.netdisc.service.CurdService;
import cn.jasonone.hm.netdisc.service.UserInfoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.annotation.Resource;

/**
 * @author Jason
 * @date 2020-07-22 14:56:57
 */
@Controller
@RequestMapping("/user")
public class UserInfoController extends CurdController<UserInfo,UserInfoDTO, UserInfoITO,Integer> {
	@Resource
	private UserInfoService userInfoService;
	@Override
	public String getModelName() {
		return "userInfo";
	}
	
	@Override
	public CurdService<UserInfo, Integer> getCurdService() {
		return userInfoService;
	}
	
	@Override
	public Class<?> getVoType() {
		return UserInfoVO.class;
	}
}
