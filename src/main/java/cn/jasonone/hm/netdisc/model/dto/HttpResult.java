package cn.jasonone.hm.netdisc.model.dto;

import com.github.pagehelper.PageInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author Jason
 * @date 2020-07-22 14:17:44
 */
public class HttpResult extends HashMap<String, Object> {
	private static final String CODE_NAME = "code";
	
	private static final String MSG_NAME = "msg";
	private static final String DATA_NAME = "data";
	
	private static final String DATA_LIST_SUFFIX = "_list";
	private static final String DATA_MAP_SUFFIX = "_map";
	
	public HttpResult() {
		this(0, null);
	}
	
	public HttpResult(String msg) {
		this(0, msg);
	}
	
	public HttpResult(int code) {
		this(code, null);
	}
	
	public HttpResult(int code, String msg) {
		this.setCode(code);
		this.setMsg(msg);
	}
	
	public HttpResult setCode(int code) {
		this.put(CODE_NAME, code);
		return this;
	}
	
	public int getCode() {
		return this.getAttribute(CODE_NAME);
	}
	
	public HttpResult setMsg(String msg) {
		this.put(MSG_NAME, msg);
		return this;
	}
	
	public String getMsg() {
		return this.getAttribute(MSG_NAME);
	}
	
	public <T> T getAttribute(String name) {
		return (T) this.get(name);
	}
	
	public <T> T getData() {
		return this.getAttribute(DATA_NAME);
	}
	
	public List<Object> getDataForArray() {
		Object data = getData();
		if (data instanceof List) {
			return (List<Object>) data;
		}
		return this.getAttribute(DATA_NAME + DATA_LIST_SUFFIX);
	}
	
	public Map<String, Object> getDataForMap() {
		Object data = getData();
		if (data instanceof Map) {
			return (Map<String, Object>) data;
		}
		return this.getAttribute(DATA_NAME + DATA_MAP_SUFFIX);
	}
	
	public HttpResult addDataAttribute(String name, Object value) {
		Map<String, Object> data = null;
		if (!this.containsKey(DATA_NAME)) {
			this.put(DATA_NAME, new HashMap<>());
			data = this.getAttribute(DATA_NAME);
		} else {
			if (this.getAttribute(DATA_NAME) instanceof Map) {
				data = this.getAttribute(DATA_NAME);
			} else {
				data = this.getAttribute(DATA_NAME + DATA_MAP_SUFFIX);
			}
		}
		
		data.put(name, value);
		return this;
	}
	
	public HttpResult addDataAttribute(Object value) {
		List<Object> data = null;
		if (!this.containsKey(DATA_NAME)) {
			this.put(DATA_NAME, new ArrayList<>());
			data = this.getAttribute(DATA_NAME);
		} else {
			if (this.getAttribute(DATA_NAME) instanceof Map) {
				data = this.getAttribute(DATA_NAME);
			} else {
				data = this.getAttribute(DATA_NAME + DATA_LIST_SUFFIX);
			}
		}
		data.add(value);
		return this;
	}
	
	public HttpResult addAttribute(String name, Object value) {
		this.put(name, value);
		return this;
	}
	
	public HttpResult setData(Object data) {
		this.addAttribute(DATA_NAME, data);
		return this;
	}
	
	public static HttpResult success(String msg, PageInfo<?> pageInfo) {
		return new HttpResult(msg).addAttribute("count", pageInfo.getTotal()).addDataAttribute(pageInfo.getList());
	}
	
	public static HttpResult success(String msg) {
		return new HttpResult(msg);
	}
	
	public static HttpResult success(String msg, Object data) {
		return new HttpResult(msg).setData(data);
	}
	
	public static HttpResult create(int code, String msg) {
		return new HttpResult(code, msg);
	}
	
	public static HttpResult create(int code, String msg, Object data) {
		return new HttpResult(code, msg).setData(data);
	}
}
