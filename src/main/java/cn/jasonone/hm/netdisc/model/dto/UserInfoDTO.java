package cn.jasonone.hm.netdisc.model.dto;

import cn.jasonone.hm.netdisc.Entity;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Email;
import javax.validation.constraints.Pattern;

/**
 * @author Jason
 * @date 2020-07-22 15:50:38
 */
@Data
public class UserInfoDTO extends UserInfo {
	/** 主键 */
	private Integer id ;
	/** 昵称;用户的昵称 */
	private String nickName ;
	/** 账号;登录使用的账号 */
	private String account ;
	/** 密码;登录使用的密码,存储密文 */
	private String password ;
	/** 邮箱;用户邮箱 */
	@Email(message = "非法的邮箱地址")
	private String email ;
	/** 头像;头像ID */
	private Integer avatar ;
	/** 身份证;用户的身份证信息 */
	@Entity
	private String identity;
	/** 状态;用户的状态 */
	@Range(min = 0,max = 4)
	private Integer state ;
}
