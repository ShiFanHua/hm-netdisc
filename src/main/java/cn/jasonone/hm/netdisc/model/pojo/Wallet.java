package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 钱包信息
 * @author Jason
 * @date 2020-07-20 11:21:18
 */
@Data
public class Wallet {
	/** 用户ID */
	private Integer userId ;
	/** 余额;钱包余额 */
	private Double balance ;
	/** 状态;状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Boolean logicallyDelete ;
	/** 更新时间 */
	private Date updatedTime ;
}
