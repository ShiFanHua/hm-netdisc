package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 会员信息
 * @author Jason
 * @date 2020-07-20 11:19:53
 */
@Data
public class MemberInformation {
	/** 用户ID */
	private Integer userId ;
	/** 会员类型;会员类型 */
	private Integer memberInformationType ;
	/** 经验;会员当前产生的经验总额 */
	private Integer experience ;
	/** 状态;状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Integer logicallyDelete ;
	/** 首次开通时间;首次开通会员的时间 */
	private Date firstTime ;
	/** 最后一次开通时间;最近一次开通会员的时间 */
	private Date lastTime ;
}
