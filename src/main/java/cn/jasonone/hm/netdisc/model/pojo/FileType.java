package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 文件类型
 * @author Jason
 * @date 2020-07-20 11:22:57
 */
@Data
public class FileType {
	/** 主键;标识号 */
	private Integer id ;
	/** 文件魔数;文件魔数 */
	private String magicNumber ;
	/** 类型名称;文件类型名称 */
	private String name ;
	/** 图标;文件类型图标 */
	private Integer icon ;
	/** 状态;状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Integer logicallyDelete ;
	/** 创建时间 */
	private Date createdTime ;
	/** 更新时间 */
	private Date updatedTime ;
	/**
	 * 文件类型图标
	 */
	private DataDictionary iconInfo;
}
