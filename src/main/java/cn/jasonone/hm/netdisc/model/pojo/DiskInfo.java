package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

import java.util.Date;
import java.util.List;

/**
 * 磁盘信息
 * @author Jason
 * @date 2020-07-20 11:22:07
 */
@Data
public class DiskInfo {
	/** 标识符;文件ID */
	private Integer id ;
	/** 用户ID;唯一标识 */
	private Integer userId ;
	/** 父文件;所属父目录 */
	private Integer pid ;
	/** 文件名;文件名 */
	private String name ;
	/**
	 * 文件路径
	 */
	private String path ;
	/** 文件类型;文件类型 */
	private Integer type ;
	/** 是否是目录;是否是目录 */
	private Boolean directory ;
	/** 文件大小;文件的大小 */
	private Long size ;
	/** 状态;状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Boolean logicallyDelete ;
	/** 创建时间 */
	private Date createdTime ;
	/** 更新时间 */
	private Date updatedTime ;
	
	private FileType fileType;
	
	private List<Integer> types;
	
	private List<DiskInfo> files;
}
