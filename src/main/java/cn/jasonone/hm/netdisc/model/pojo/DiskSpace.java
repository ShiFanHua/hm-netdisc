package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

import java.util.Date;

/**
 * 磁盘空间信息
 * @author Jason
 * @date 2020-07-20 11:24:12
 */
@Data
public class DiskSpace {
	/** 主键;唯一标识 */
	private Integer id ;
	/** 用户ID;空间所属用户 */
	private Integer userId ;
	/** 空间大小;空间大小 */
	private Long size ;
	/** 空间类型;空间类型 */
	private Integer type ;
	/** 状态;状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Integer logicallyDelete ;
	/** 到期时间;空间到期时间 */
	private Date expireDate ;
	/** 创建时间 */
	private Date createdTime ;
}
