package cn.jasonone.hm.netdisc.model.dto;

import cn.jasonone.hm.netdisc.Entity;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;
import lombok.Data;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author Jason
 * @date 2020-07-22 15:50:38
 */
@Data
public class UserInfoITO extends UserInfo {
	/** 昵称;用户的昵称 */
	@NotEmpty(message = "用户昵称不能为空")
	private String nickName ;
	/** 账号;登录使用的账号 */
	@NotEmpty(message = "用户账号不能为空")
	private String account ;
	/** 密码;登录使用的密码,存储密文 */
	@NotEmpty(message = "用户密码不能为空")
	private String password ;
	/** 邮箱;用户邮箱 */
	@NotEmpty(message = "用户邮箱不能为空")
	@Email(message = "非法的邮箱")
	private String email ;
	/** 头像;头像ID */
	@NotNull(message = "用户头像不能为空")
	private Integer avatar ;
	/** 身份证;用户的身份证信息 */
	@NotEmpty(message = "用户身份证不能为空")
	@Entity
	private String identity;
}
