package cn.jasonone.hm.netdisc.model.pojo;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;


/**
 * 用户信息
 * @author Jason
 * @date 2020-07-20 11:17:29
 */
@Data
public class UserInfo {
	/** 主键 */
	private Integer id ;
	/** 昵称;用户的昵称 */
	private String nickName ;
	/** 账号;登录使用的账号 */
	private String account ;
	/** 密码;登录使用的密码,存储密文 */
	private String password ;
	/** 邮箱;用户邮箱 */
	private String email ;
	/** 头像;头像ID */
	private Integer avatar ;
	/** 身份证;用户的身份证信息 */
	private String identity;
	/** 性别;用户性别 */
	private Integer sex ;
	/** 籍贯;用户的籍贯 */
	private String nativePlace ;
	/** 出生日期;用户的出生日期 */
	@JsonFormat(pattern = "yyyy-MM-dd")
	private Date dateBirth ;
	/** 状态;用户的状态 */
	private Integer state ;
	/** 逻辑删除;逻辑删除字段,1为删除 */
	private Boolean logicallyDelete ;
	/** 注册时间;用户的注册时间 */
	private Date createdTime ;
	/** 更新时间;用户的最后修改时间 */
	private Date updatedTime ;
}
