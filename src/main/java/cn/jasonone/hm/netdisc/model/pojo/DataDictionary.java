package cn.jasonone.hm.netdisc.model.pojo;

import lombok.Data;

/**
 * 数据字典信息
 * @author Jason
 * @date 2020-07-20 11:23:30
 */
@Data
public class DataDictionary {
	/** 主键;唯一标识 */
	private Integer id ;
	/** 分组;数据分组 */
	private String code ;
	/** 名称;数据名称 */
	private String name ;
	/** 数据;数据的值 */
	private String value ;
}
