package cn.jasonone.hm.netdisc.config;

import cn.jasonone.hm.netdisc.interceptor.LoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.annotation.Resource;

/**
 * @author Jason
 * @date 2020-07-23 15:34:07
 */
@Configuration
public class WebConfig implements WebMvcConfigurer {
	@Resource
	private LoginInterceptor loginInterceptor;
	
	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		InterceptorRegistration interceptor = registry.addInterceptor(loginInterceptor);
		interceptor.excludePathPatterns("/layui/**","/register","/login","/user/add");
	}
}
