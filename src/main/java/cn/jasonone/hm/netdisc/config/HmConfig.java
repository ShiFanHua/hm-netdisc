package cn.jasonone.hm.netdisc.config;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.util.List;
import java.util.Map;

/**
 * @author Jason
 * @date 2020-07-31 14:39:32
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "hm-netdisc")
public class HmConfig {
	/**
	 * 文件上传存放的根目录
	 */
	private String fileUploadRoot;
	/**
	 * 文件类型分组
	 */
	private Map<String, List<String>> fileTypeGroups;
}
