package cn.jasonone.hm.netdisc.interceptor;

import cn.jasonone.hm.netdisc.consts.SystemConst;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 * @author Jason
 * @date 2020-07-23 15:34:52
 */
@Component
public class LoginInterceptor implements HandlerInterceptor {
	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
		HttpSession session = request.getSession();
		if(session.getAttribute(SystemConst.SESSION_LOGIN_STATUS) == null){
			response.sendRedirect(request.getContextPath()+"/login");
			return false;
		}
		return true;
	}
}
