package cn.jasonone.hm.netdisc.mapper;

import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import org.apache.ibatis.annotations.Param;

public interface RecycleMapper extends CurdMapper<DiskInfo,Integer> {
    void recover(@Param("ids") Integer... ids);
}
