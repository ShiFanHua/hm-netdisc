package cn.jasonone.hm.netdisc.mapper;

import cn.jasonone.hm.netdisc.model.pojo.FileType;

import java.util.List;

/**
 * @author monster
 * @create 2020-07-26 15:38
 */
public interface FileTypeMapper extends CurdMapper<FileType,Integer> {

}
