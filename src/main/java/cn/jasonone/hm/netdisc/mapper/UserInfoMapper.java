package cn.jasonone.hm.netdisc.mapper;

import cn.jasonone.hm.netdisc.model.pojo.UserInfo;

/**
 * 用户持久化对象
 * @author Jason
 * @date 2020-07-20 11:44:16
 */
public interface UserInfoMapper extends CurdMapper<UserInfo,Integer> {
	UserInfo login(UserInfo userInfo);
}
