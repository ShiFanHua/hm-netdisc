package cn.jasonone.hm.netdisc.mapper;

import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Jason
 * @date 2020-07-24 14:06:51
 */
public interface DiskInfoMapper extends CurdMapper<DiskInfo,Integer> {

	public DiskInfo getDirectory(@Param("name")String name,@Param("pid") Integer pid,@Param("userId") Integer userId);
	
	public List<DiskInfo> findAllById(@Param("ids") Integer ...ids);
}
