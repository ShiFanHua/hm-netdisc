package cn.jasonone.hm.netdisc.mapper;

import cn.jasonone.hm.netdisc.model.pojo.DataDictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Jason
 * @date 2020-07-27 10:33:23
 */
public interface DataDictionaryMapper extends CurdMapper<DataDictionary,Integer> {

	List<DataDictionary> findByCode(@Param("code")String code);
	
	DataDictionary findByValue(@Param("code") String code,@Param("value") String value);
	
	DataDictionary findByName(@Param("code")String code,@Param("name")String name);
	
	List<DataDictionary> findByNames(@Param("code")String code,@Param("names")Iterable<String> names);

}
