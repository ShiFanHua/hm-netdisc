package cn.jasonone.hm.netdisc.mapper;

import org.apache.ibatis.annotations.Param;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * 钱包的增删改查接口
 * @author Yangziming
 * @date 2020-07-24 11:37:20
 */
public interface WalletMapper <Wallet,Integer>{
    /**
     * 通过用户id查询钱包对象
     * @param userId 用户id
     * @return 钱包对象
     */
    Wallet findById(@Param("userId") Integer userId);

    /**
     * 使用钱包基本信息查询
     * @param t 钱包信息对象
     * @return 结果集合
     */
    List<Wallet> findAll(Wallet t);

    /**
     * 保存对象信息
     * 当对象的注解不为空时,则为新增,否则为修改
     * @param t 需要保存的对象
     * @return 受影响行数
     */
    int save(Wallet t);

    /**
     * 根据主键删除钱包对象
     * @param logical 是否是逻辑删除
     * @param ids 主键列表
     * @return 受影响行数
     */
    int deleteByIds(@Param("logical") boolean logical,@Param("ids")Integer ...ids);

    /**
     * 根据钱包基本信息进行删除
     * @param logical 是否是逻辑删除
     * @param t 钱包信息对象
     * @return 受影响行数
     */
    int delete(@Param("logical") boolean logical,Wallet t);


}
