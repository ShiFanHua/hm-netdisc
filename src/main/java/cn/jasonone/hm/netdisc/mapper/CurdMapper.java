package cn.jasonone.hm.netdisc.mapper;

import org.apache.ibatis.annotations.Param;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * 基本增删改查接口基类
 * @author Jason
 * @date 2020-07-20 11:37:20
 */
public interface CurdMapper<T,ID> {
	/**
	 * 通过主键查询对象
	 * @param id 注解
	 * @return 对象信息
	 */
	T findById(@PathParam("id") ID id);
	
	/**
	 * 使用基本条件进行查询
	 * @param t 条件对象
	 * @return 查询结果列表
	 */
	List<T> findAll(T t);
	
	/**
	 * 保存对象信息.
	 * <p>
	 *     当对象的注解不为空时,则为新增,否则为修改
	 * </p>
	 * @param t 需要保存的对象
	 * @return 受影响行
	 */
	int save(T t);
	
	/**
	 * 根据主键删除对象信息
	 * @param logical 是否为逻辑删除
	 * @param ids 主键列表
	 * @return 受影响行
	 */
	int deleteByIds(@Param("logical") boolean logical,@Param("ids") Integer ...ids);
	
	/**
	 * 根据基本条件进行删除
	 * @param logical 是否为逻辑删除
	 * @param t 基本条件对象
	 * @return 受影响行
	 */
	int delete(@Param("logical") boolean logical,@Param("entity") T t);
}
