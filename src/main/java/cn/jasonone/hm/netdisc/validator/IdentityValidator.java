package cn.jasonone.hm.netdisc.validator;

import cn.jasonone.hm.netdisc.Entity;
import cn.jasonone.hm.netdisc.util.IdentityUtil;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

/**
 * @author Jason
 * @date 2020-07-22 15:58:50
 */
public class IdentityValidator implements ConstraintValidator<Entity,CharSequence> {
	
	@Override
	public boolean isValid(CharSequence value, ConstraintValidatorContext context) {
		if(value == null){
			return true;
		}
		String identity=value.toString();
		if(identity.length() == 18 || identity.length() == 15){
			if(identity.length() == 15&&identity.matches("\\d{15}")){
				return true;
			}else{
				IdentityUtil util=new IdentityUtil(identity);
				return util.isValidator();
			}
//			430123801001123
//			43012319801001123x
		}
		return false;
	}
}
