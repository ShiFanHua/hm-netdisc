package cn.jasonone.hm.netdisc.consts;

/**
 * @author Jason
 * @date 2020-07-23 15:32:25
 */
public interface SystemConst {
	/**
	 * 当前登录用户KEY
	 */
	String SESSION_LOGIN_STATUS="LOGIN_STATUS";
}
