package cn.jasonone.hm.netdisc.util;

import cn.hutool.core.util.ReflectUtil;
import org.springframework.beans.BeanUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jason
 * @date 2020-07-23 14:29:14
 */
public class DTOUtil {
	/**
	 * 将持久化对象转换为视图对象
	 * @param po 持久化对象
	 * @param voType 视图对象类型
	 * @param <T> 视图对象类型
	 * @return 视图对象
	 */
	public static <T> T toVO(Object po,Class<T> voType){
		T vo = ReflectUtil.newInstance(voType);
		BeanUtils.copyProperties(po, vo);
		return vo;
	}
	
	public static <T> List<T> toVo(Iterable<?> iterable,Class<T> voType){
		List<T> list = new ArrayList<>();
		for (Object o : iterable) {
			list.add(toVO(o, voType));
		}
		return list;
	}
}
