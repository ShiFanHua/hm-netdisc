package cn.jasonone.hm.netdisc.service;

import cn.jasonone.hm.netdisc.model.pojo.FileType;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;

/**
 * @author monster
 * @create 2020-07-24 12:09
 */
public interface FileTypeService extends CurdService<FileType,Integer> {
	/**
	 * 解析文件类型
	 * @param headers 需要文件头64字节数据
	 * @return 文件类型对象
	 */
	public FileType parseFileType(byte[] headers);
}
