package cn.jasonone.hm.netdisc.service;

import com.github.pagehelper.PageInfo;
import org.apache.ibatis.annotations.Param;

import javax.websocket.server.PathParam;
import java.util.List;

/**
 * 基本增删改查业务基类
 * @author Jason
 * @date 2020-07-20 12:22:16
 */
public interface CurdService<T,ID> {
	/**
	 * 通过主键查询对象
	 * @param id 注解
	 * @return 对象信息
	 */
	T findById( ID id);
	
	/**
	 * 使用基本条件进行查询
	 * @param t 条件对象
	 * @return 查询结果列表
	 */
	List<T> findAll(T t);
	
	/**
	 * 基于基本条件进行分页查询
	 * @param t 条件对象
	 * @param page 页码
	 * @param limit 每页数据量
	 * @return 分页对象,参照 {@link PageInfo}
	 */
	PageInfo<T> findAll(T t,int page,int limit);
	
	
	/**
	 * 保存对象信息.
	 * <p>
	 *     当对象的注解不为空时,则为新增,否则为修改
	 * </p>
	 * @param t 需要保存的对象
	 */
	void save(T t);
	
	/**
	 * 根据主键删除对象信息
	 * @param logical 是否为逻辑删除
	 * @param ids 主键列表
	 */
	void deleteByIds(boolean logical, Integer ...ids);
	
	/**
	 * 根据基本条件进行删除
	 * @param logical 是否为逻辑删除
	 * @param t 基本条件对象
	 */
	void delete(boolean logical,T t);
	
	
}
