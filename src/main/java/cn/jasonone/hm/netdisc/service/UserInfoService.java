package cn.jasonone.hm.netdisc.service;

import cn.jasonone.hm.netdisc.model.pojo.UserInfo;

/**
 * 用户业务接口
 * @author Jason
 * @date 2020-07-20 12:26:14
 */
public interface UserInfoService extends CurdService<UserInfo,Integer> {
	UserInfo login(UserInfo userInfo);
}
