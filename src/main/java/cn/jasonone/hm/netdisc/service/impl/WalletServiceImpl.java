package cn.jasonone.hm.netdisc.service.impl;

import cn.jasonone.hm.netdisc.mapper.WalletMapper;
import cn.jasonone.hm.netdisc.model.pojo.Wallet;
import cn.jasonone.hm.netdisc.service.WalletService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;

import javax.annotation.Resource;
import java.util.List;
/**
 * 钱包业务对象
 * @author Yangziming
 * @date 2020-07-24 12:26:34
 */
public class WalletServiceImpl implements WalletService<Wallet,Integer> {
    @Resource
    private WalletMapper walletMapper;

    @Override
    public Wallet findById(Integer userId) {
        return (Wallet) walletMapper.findById(userId);
    }

    @Override
    public List<Wallet> findAll(Wallet wallet) {
        if (wallet==null){
            wallet=new Wallet();
            wallet.setLogicallyDelete(false);
            wallet.setState(0);
        }else{
            if (wallet.getLogicallyDelete()==null){
                wallet.setLogicallyDelete(false);
            }
            if (wallet.getState()==null){
                wallet.setState(0);
            }
        }
        return walletMapper.findAll(wallet);
    }

    @Override
    public PageInfo<Wallet> findAll(Wallet wallet, int page, int limit) {
        PageHelper.startPage(page,limit);
        List<Wallet> wallets=this.walletMapper.findAll(wallet);
        return new PageInfo<>(wallets);
    }

    @Override
    public void save(Wallet wallet) {
       if (wallet.getState()==null){
           wallet.setState(0);
       }
       if (wallet.getLogicallyDelete()==null){
           wallet.setLogicallyDelete(false);
       }
       this.walletMapper.save(wallet);
    }


    @Override
    public void deleteByIds(boolean logical, Integer... ids) {
        this.walletMapper.deleteByIds(logical,ids);
    }

    @Override
    public void delete(boolean logical, Wallet wallet) {
       this.walletMapper.delete(logical,wallet);
    }
}
