package cn.jasonone.hm.netdisc.service.impl;

import cn.jasonone.hm.netdisc.mapper.RecycleMapper;
import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import cn.jasonone.hm.netdisc.service.RecycleService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class RecycleServiceImpl implements RecycleService {
    @Resource
    private RecycleMapper recycleMapper;
    @Override
    public DiskInfo findById(Integer integer) {
        return recycleMapper.findById(integer);
    }

    @Override
    public List<DiskInfo> findAll(DiskInfo diskInfo) {
        if (diskInfo == null) {
            diskInfo=new DiskInfo();
            diskInfo.setLogicallyDelete(true);
            diskInfo.setState(0);
        }else{
            if(diskInfo.getLogicallyDelete() == null){
                diskInfo.setLogicallyDelete(true);
            }
            if(diskInfo.getState() == null){
                diskInfo.setState(0);
            }
        }
        diskInfo.setPid(null);
        return this.recycleMapper.findAll(diskInfo);
    }

    @Override
    public PageInfo<DiskInfo> findAll(DiskInfo diskInfo, int page, int limit) {
        PageHelper.startPage(page, limit);
        List<DiskInfo> userInfos = this.recycleMapper.findAll(diskInfo);
        return new PageInfo<>(userInfos);
    }

    @Override
    public void save(DiskInfo diskInfo) {

    }

    @Override
    public void deleteByIds(boolean logical, Integer... ids) {
        this.recycleMapper.deleteByIds(logical, ids);
    }

    @Override
    public void delete(boolean logical, DiskInfo diskInfo) {
        this.recycleMapper.delete(logical, diskInfo);
    }

    @Override
    public void recover(Integer... ids) {
        log.debug("RecycleServiceImpl(recover ids):"+Arrays.toString(ids));
        this.recycleMapper.recover(ids);
    }
}
