package cn.jasonone.hm.netdisc.service;

import cn.jasonone.hm.netdisc.model.pojo.DataDictionary;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @author Jason
 * @date 2020-07-31 14:30:16
 */
public interface DataDictionaryService extends CurdService<DataDictionary,Integer> {
	List<DataDictionary> findByNames(String code,Iterable<String> names);
}
