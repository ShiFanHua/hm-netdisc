package cn.jasonone.hm.netdisc.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.jasonone.hm.netdisc.mapper.UserInfoMapper;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;
import cn.jasonone.hm.netdisc.service.UserInfoService;
import cn.jasonone.hm.netdisc.util.IdentityUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户业务对象
 * @author Jason
 * @date 2020-07-20 12:26:34
 */
@Service
public class UserInfoServiceImpl implements UserInfoService {
	@Resource
	private UserInfoMapper userInfoMapper;
	@Override
	public UserInfo findById(Integer integer) {
		return userInfoMapper.findById(integer);
	}
	
	@Override
	public List<UserInfo> findAll(UserInfo userInfo) {
		if (userInfo == null) {
			userInfo=new UserInfo();
			userInfo.setLogicallyDelete(false);
			userInfo.setState(0);
		}else{
			if(userInfo.getLogicallyDelete() == null){
				userInfo.setLogicallyDelete(false);
			}
			if(userInfo.getState() == null){
				userInfo.setState(0);
			}
		}
		return userInfoMapper.findAll(userInfo);
	}
	
	@Override
	public PageInfo<UserInfo> findAll(UserInfo userInfo, int page, int limit) {
		PageHelper.startPage(page, limit);
		List<UserInfo> userInfos = this.userInfoMapper.findAll(userInfo);
		return new PageInfo<>(userInfos);
	}
	
	@Override
	public void save(UserInfo userInfo) {
		IdentityUtil iu=new IdentityUtil(userInfo.getIdentity());
		userInfo.setSex(iu.getSex());
		userInfo.setDateBirth(DateUtil.parse(iu.getBirthDay(), "yyyy-MM-dd"));
		userInfo.setNativePlace(iu.getNativePlace());
		if(userInfo.getState() == null){
			userInfo.setState(0);
		}
		if(userInfo.getLogicallyDelete() == null){
			userInfo.setLogicallyDelete(false);
		}
		this.userInfoMapper.save(userInfo);
	}
	
	@Override
	public void deleteByIds(boolean logical, Integer... ids) {
		this.userInfoMapper.deleteByIds(logical, ids);
	}
	
	@Override
	public void delete(boolean logical, UserInfo userInfo) {
		this.userInfoMapper.delete(logical, userInfo);
	}
	
	@Override
	public UserInfo login(UserInfo userInfo) {
		return this.userInfoMapper.login(userInfo);
	}
}
