package cn.jasonone.hm.netdisc.service;

import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import cn.jasonone.hm.netdisc.model.pojo.UserInfo;

import java.util.List;

/**
 * 用户业务接口
 * @author Jason
 * @date 2020-07-20 12:26:14
 */
public interface DiskInfoService extends CurdService<DiskInfo,Integer> {
	public DiskInfo getDirectory(String name,Integer pid,Integer userId);
	
	public List<DiskInfo> findAllById(Integer ...ids);
}
