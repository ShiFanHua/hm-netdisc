package cn.jasonone.hm.netdisc.service.impl;

import cn.jasonone.hm.netdisc.mapper.DataDictionaryMapper;
import cn.jasonone.hm.netdisc.model.pojo.DataDictionary;
import cn.jasonone.hm.netdisc.service.DataDictionaryService;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户业务对象
 * @author Jason
 * @date 2020-07-20 12:26:34
 */
@Service
public class DataDictionaryServiceImpl implements DataDictionaryService {
	@Resource
	private DataDictionaryMapper dataDictionaryMapper;
	@Override
	public DataDictionary findById(Integer integer) {
		return dataDictionaryMapper.findById(integer);
	}
	
	@Override
	public List<DataDictionary> findAll(DataDictionary dataDictionary) {
		return dataDictionaryMapper.findAll(dataDictionary);
	}
	
	@Override
	public PageInfo<DataDictionary> findAll(DataDictionary dataDictionary, int page, int limit) {
		PageHelper.startPage(page, limit);
		List<DataDictionary> userInfos = this.dataDictionaryMapper.findAll(dataDictionary);
		return new PageInfo<>(userInfos);
	}
	
	@Override
	public void save(DataDictionary dataDictionary) {
		this.dataDictionaryMapper.save(dataDictionary);
	}
	
	@Override
	public void deleteByIds(boolean logical, Integer... ids) {
		this.dataDictionaryMapper.deleteByIds(logical, ids);
	}
	
	@Override
	public void delete(boolean logical, DataDictionary dataDictionary) {
		this.dataDictionaryMapper.delete(logical, dataDictionary);
	}
	
	
	@Override
	public List<DataDictionary> findByNames(String code, Iterable<String> names) {
		return this.dataDictionaryMapper.findByNames(code, names);
	}
}
