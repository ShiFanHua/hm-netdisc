package cn.jasonone.hm.netdisc.service;

import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;

public interface RecycleService extends CurdService<DiskInfo,Integer> {
    void recover(Integer... ids);
}
