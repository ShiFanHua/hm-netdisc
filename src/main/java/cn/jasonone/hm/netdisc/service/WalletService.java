package cn.jasonone.hm.netdisc.service;

import com.github.pagehelper.PageInfo;

import java.util.List;
/**
 * 钱包业务接口
 * @author Yangziming
 * @date 2020-07-24 12:26:14
 */
public interface WalletService<Wallet,Integer>{
    /**
     * 通过用户id查询钱包对象
     * @param userId 用户id
     * @return 钱包对象
     */
    Wallet findById(Integer userId);

    /**
     * 使用钱包基本信息查询
     * @param wallet 钱包信息对象
     * @return 结果集合
     */
    List<Wallet> findAll(Wallet wallet);

    /**
     * 基于钱包信息分页查询
     * @param wallet 钱包信息对象
     * @param page 页码
     * @param limit 每页数据量
     * @return 分页对象,参照 {@link PageInfo}
     */
    PageInfo<Wallet> findAll(Wallet wallet,int page,int limit);

    /**
     * 保存对象信息.
     * <p>
     *     当对象的注解不为空时,则为新增,否则为修改
     * </p>
     * @param wallet 需要保存的对象
     */
    void save(Wallet wallet);

    /**
     * 根据主键删除钱包对象
     * @param logical 是否是逻辑删除
     * @param ids 主键列表
     * @return 受影响行数
     */
    void deleteByIds(boolean logical,Integer ...ids);

    /**
     * 根据钱包基本信息进行删除
     * @param logical 是否是逻辑删除
     * @param wallet 钱包信息对象
     * @return 受影响行数
     */
    void delete(boolean logical,Wallet wallet);
}
