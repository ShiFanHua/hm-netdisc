package cn.jasonone.hm.netdisc.service.impl;

import cn.hutool.core.date.DateUtil;
import cn.jasonone.hm.netdisc.mapper.DiskInfoMapper;
import cn.jasonone.hm.netdisc.model.pojo.DiskInfo;
import cn.jasonone.hm.netdisc.service.DiskInfoService;
import cn.jasonone.hm.netdisc.util.IdentityUtil;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用户业务对象
 * @author Jason
 * @date 2020-07-20 12:26:34
 */
@Service
public class DiskInfoServiceImpl implements DiskInfoService {
	@Resource
	private DiskInfoMapper diskInfoMapper;
	@Override
	public DiskInfo findById(Integer integer) {
		return diskInfoMapper.findById(integer);
	}
	
	@Override
	public List<DiskInfo> findAll(DiskInfo diskInfo) {
		if (diskInfo == null) {
			diskInfo=new DiskInfo();
			diskInfo.setLogicallyDelete(false);
			diskInfo.setState(0);
		}else{
			if(diskInfo.getLogicallyDelete() == null){
				diskInfo.setLogicallyDelete(false);
			}
			if(diskInfo.getState() == null){
				diskInfo.setState(0);
			}
		}
		return diskInfoMapper.findAll(diskInfo);
	}
	
	@Override
	public PageInfo<DiskInfo> findAll(DiskInfo diskInfo, int page, int limit) {
		PageHelper.startPage(page, limit);
		List<DiskInfo> userInfos = this.diskInfoMapper.findAll(diskInfo);
		return new PageInfo<>(userInfos);
	}
	
	@Override
	public void save(DiskInfo diskInfo) {
		if(diskInfo.getState() == null){
			diskInfo.setState(0);
		}
		if(diskInfo.getLogicallyDelete() == null){
			diskInfo.setLogicallyDelete(false);
		}
		this.diskInfoMapper.save(diskInfo);
	}
	
	@Override
	public void deleteByIds(boolean logical, Integer... ids) {
		this.diskInfoMapper.deleteByIds(logical, ids);
	}
	
	@Override
	public void delete(boolean logical, DiskInfo diskInfo) {
		this.diskInfoMapper.delete(logical, diskInfo);
	}
	
	@Override
	public DiskInfo getDirectory(String name, Integer pid, Integer userId) {
		return this.diskInfoMapper.getDirectory(name, pid, userId);
	}
	
	@Override
	public List<DiskInfo> findAllById(Integer... ids) {
		return diskInfoMapper.findAllById(ids);
	}
}
