package cn.jasonone.hm.netdisc;

import cn.jasonone.hm.netdisc.validator.IdentityValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.lang.reflect.Field;

/**
 * 身份证验证注解
 * @author Jason
 * @date 2020-07-22 15:59:41
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Constraint(validatedBy = IdentityValidator.class)
public @interface Entity {
	String message() default "非法的身份证";
	
	Class<?>[] groups() default { };
	
	Class<? extends Payload>[] payload() default { };
}
