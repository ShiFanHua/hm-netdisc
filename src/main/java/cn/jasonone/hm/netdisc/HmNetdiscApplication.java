package cn.jasonone.hm.netdisc;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@MapperScan("cn.jasonone.hm.netdisc.mapper")
public class HmNetdiscApplication {
	
	public static void main(String[] args) {
		SpringApplication.run(HmNetdiscApplication.class, args);
	}
	
}
